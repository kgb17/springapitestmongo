package com.caleroPortfolio.springMongo.controllers;

import com.caleroPortfolio.springMongo.model.Product;
import com.caleroPortfolio.springMongo.repositories.ProductRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/Products")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public Iterable<Product> getAllProducts(){
        return productRepository.findAll();
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public Product getProductById(@PathVariable("id")ObjectId id){
        return productRepository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public void modifyProductById(@PathVariable("id") ObjectId id, @Valid @RequestBody Product product){
        product.set_id(id);
        productRepository.save(product);
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String saveProduct(@Valid @RequestBody Product product){
        productRepository.save(product);
        return product.get_id();
    }

}
