package com.caleroPortfolio.springMongo.repositories;

import com.caleroPortfolio.springMongo.model.Product;
import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product,String> {

    @Override
    void delete(Product deleted);
    Product findBy_id(ObjectId _id);

}
